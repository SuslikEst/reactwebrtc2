import Head from 'next/head'
import { Welcome } from './components/steps/Welcome'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Welcome />
    </div>
  )
}
